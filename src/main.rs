use std::{
    io,
    time::{Duration, Instant},
};

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    backend::{Backend, CrosstermBackend},
    layout::{Constraint, Direction, Layout},
    style::Style,
    widgets::{Block, Borders, Gauge},
    Frame, Terminal,
};
use time::OffsetDateTime;

mod backend;
use backend::{get_color, get_intervals, get_seconds, notify_time, stylize_seconds, Intervals};

enum Interval {
    Long,
    Short,
}

struct App {
    progress: u16,
    remaining: i64,
    intervals: Intervals,
}

fn main() -> Result<(), io::Error> {
    let intervals: Intervals = match get_intervals() {
        Some(intervals) => intervals,
        None => return Ok(()),
    };

    let app = App {
        progress: u16::default(),
        remaining: i64::default(),
        intervals,
    };

    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let tick_rate = Duration::from_millis(250);
    let res = run_app(&mut terminal, app, tick_rate);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{err:?}");
    }

    Ok(())
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    tick_rate: Duration,
) -> io::Result<()> {
    let mut last_tick = Instant::now();
    let mut current_interval = Interval::Long;
    let mut pre_pause_start_time = OffsetDateTime::now_utc().time();
    let mut offset_start_time = pre_pause_start_time;

    let mut pause: bool = false;
    let mut paused: bool = false;

    loop {
        // Gather time information
        let duration = match current_interval {
            Interval::Short => &app.intervals.short,
            Interval::Long => &app.intervals.long,
        };
        let time_duration = time::Duration::seconds(get_seconds(*duration));
        let mut current_time = OffsetDateTime::now_utc().time();
        let remaining_time = offset_start_time + time_duration - current_time;

        app.progress = ((current_time - offset_start_time) / time_duration * 100.0) as u16;
        app.remaining = remaining_time.whole_seconds() + 1;

        // Draw UI and detect keystrokes
        terminal.draw(|f| ui(f, &app))?;
        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));

        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                if key.code == KeyCode::Char('q') {
                    return Ok(());
                }
                if let KeyCode::Char('p' | ' ') = key.code {
                    if pause {
                        pause = false;
                    } else {
                        pause = true;
                    }
                }
                if key.code == KeyCode::Char('f') {
                    current_time += remaining_time;
                }
            }
        }

        // Manage pause
        if pause && !paused {
            last_tick = Instant::now();
            pause = false;
            paused = true;
        } else if pause && paused {
            pre_pause_start_time = offset_start_time;
            pause = false;
            paused = false;
        }

        if paused {
            offset_start_time = pre_pause_start_time + last_tick.elapsed();
        }

        if current_time >= offset_start_time + time_duration {
            current_interval = match current_interval {
                Interval::Short => {
                    notify_time(app.intervals.short, app.intervals.long);
                    Interval::Long
                }
                Interval::Long => {
                    notify_time(app.intervals.long, app.intervals.short);
                    Interval::Short
                }
            };

            pre_pause_start_time = OffsetDateTime::now_utc().time();
            offset_start_time = pre_pause_start_time;
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &App) {
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints([Constraint::Percentage(100)].as_ref())
        .split(f.size());

    let gauge = Gauge::default()
        .block(Block::default().borders(Borders::NONE))
        .gauge_style(Style::default().fg(get_color(app.intervals.color.clone())))
        .percent(app.progress)
        .label(stylize_seconds(app.remaining));
    f.render_widget(gauge, chunks[0]);
}
