use std::{
    env,
    fs::{create_dir, read_to_string, File},
    io::{BufReader, ErrorKind, Write},
    num::ParseIntError,
    path::PathBuf,
};

use dirs::home_dir;
use notify_rust::{Notification, Timeout, Urgency};
use ratatui::style::Color;
use rodio::{source::Source, Decoder, OutputStream};
use serde_derive::Deserialize;

const DEFAULT_CONFIG: &str = "\
long = [0, 30, 0]
short = [0, 5, 0]
# color = \"red\"
# alarm = \"/home/username/Music/alarm.mp3\"
";

const HELP: &str = "\
USAGE:
  tomaquet [LONG_INTERVAL] [SHORT INTERVAL]

ARGUMENTS:
  LONG_INTERVAL     Formatted as hh:mm:ss
  SHORT_INTERVAL    Formatted as hh:mm:ss

FLAGS:
  -h                Show this message

SHORTCUTS:
  p                 Pause/Resume
  f                 Finish interval
  q                 Quit

INFO:
  Tomaquet will use the values set on the config file if no arguments are provided.
  Config file path: $HOME/.config/tomaquet/tomaquet.toml
";

const DEF_COLOR_ENUM: Color = Color::Red;

enum ParseError {
    ParseIntError,
    FormattingError,
}

#[derive(Deserialize)]
pub struct Intervals {
    pub long: (u8, u8, u8),
    pub short: (u8, u8, u8),
    pub color: Option<String>,
    pub alarm: Option<String>,
}

/* ---------- CONFIG FILE ---------- */

fn config_dir_path() -> PathBuf {
    let mut config_dir_path = home_dir().unwrap();
    config_dir_path.push(".config");
    config_dir_path.push("tomaquet");
    config_dir_path
}

fn config_file_path() -> PathBuf {
    let mut config_file_path = config_dir_path();
    config_file_path.push("tomaquet.toml");
    config_file_path
}

fn read_config() -> Option<Intervals> {
    let config: String = match read_to_string(config_file_path()) {
        Ok(config) => config,
        Err(err) => {
            if err.kind() == ErrorKind::NotFound {
                create_new_config();
            } else {
                println!("Error: Could not read config");
                return None;
            }
            String::from(DEFAULT_CONFIG)
        }
    };

    match toml::from_str(&config) {
        Ok(config) => config,
        Err(err) => {
            println!("Error: could not parse config {}", err.message());
            None
        }
    }
}

fn create_new_config() {
    match create_dir(config_dir_path()) {
        Ok(_) => println!("Created configuration folder."),
        Err(err) => match err.kind() {
            // If the folder already exists it will just continue.
            ErrorKind::AlreadyExists => {
                println!("Configuration folder found.");
            }
            other => panic!("Error: {other}"),
        },
    }

    let mut new_config_file =
        File::create(config_file_path()).expect("Error: Failed to create config file");
    new_config_file
        .write_all(DEFAULT_CONFIG.as_bytes())
        .expect("Error: Failed to write to config file");
    println!("Created config file");
}

/* ---------- CLI ---------- */

fn read_cli() -> Result<Intervals, bool> {
    // The bool on the Err result refers to if the program should continue or not.
    let mut cli_input = env::args();

    let _ = cli_input.next(); // The first env argument is the binary
    let long_cli = cli_input.next();
    let short_cli = cli_input.next();

    let long_str: String = match long_cli {
        Some(long_str) => {
            if long_str.contains("-h") {
                println!("{HELP}");
                return Err(false);
            }
            long_str
        }
        None => return Err(true),
    };
    let short_str: String = match short_cli {
        Some(short_str) => short_str,
        None => return Err(true),
    };

    let long = match parse_interval(&long_str) {
        Ok(long) => long,
        Err(err) => match err {
            ParseError::ParseIntError => {
                println!("Error: Couldn't parse numbers\n\n{HELP}");
                return Err(false);
            }
            ParseError::FormattingError => {
                println!("Error: Incorrect formatting\n\n{HELP}");
                return Err(false);
            }
        },
    };
    let short = match parse_interval(&short_str) {
        Ok(short) => short,
        Err(err) => match err {
            ParseError::ParseIntError => {
                println!("Error: Couldn't parse numbers\n\n{HELP}");
                return Err(false);
            }
            ParseError::FormattingError => {
                println!("Error: Incorrect formatting\n\n{HELP}");
                return Err(false);
            }
        },
    };

    Ok(Intervals {
        long,
        short,
        color: None,
        alarm: None,
    })
}

/* ---------- PARSE AND STYLIZE ---------- */

fn parse_interval(interval: &str) -> Result<(u8, u8, u8), ParseError> {
    if interval.len() == 8 && &interval[2..3] == ":" && &interval[5..6] == ":" {
        let hours: &Result<u8, ParseIntError> = &interval[0..2].parse();
        let minutes: &Result<u8, ParseIntError> = &interval[3..5].parse();
        let seconds: &Result<u8, ParseIntError> = &interval[6..8].parse();

        if hours.is_err() || minutes.is_err() || seconds.is_err() {
            Err(ParseError::ParseIntError)
        } else {
            Ok((
                hours.clone().unwrap_or_default(),
                minutes.clone().unwrap_or_default(),
                seconds.clone().unwrap_or_default(),
            ))
        }
    } else {
        Err(ParseError::FormattingError)
    }
}

fn stylize_text(time: (u8, u8, u8)) -> String {
    let (hours, minutes, seconds) = time;
    let mut stylized: String = String::new();
    if hours > 0 {
        stylized.push_str(&hours.to_string());
        stylized.push_str(" hour");
    }
    if hours > 0 && (minutes > 0 || seconds > 0) {
        stylized.push_str(", ");
    }
    if minutes > 0 {
        stylized.push_str(&minutes.to_string());
        stylized.push_str(" minute");
    }
    if minutes > 0 && seconds > 0 {
        stylized.push_str(", ");
    }
    if seconds > 0 {
        stylized.push_str(&seconds.to_string());
        stylized.push_str(" second");
    }
    stylized
}

pub fn stylize_seconds(seconds: i64) -> String {
    let hours = seconds / 3600;
    let minutes = seconds % 3600 / 60;
    let seconds = seconds % 3600 % 60;
    format!("{hours}:{minutes}:{seconds}")
}

/* ---------- API ---------- */

pub fn get_intervals() -> Option<Intervals> {
    let config = read_config();
    let color = config.as_ref().map_or_else(
        || Some(String::from("red")), // If the config file is missing, it defaults to red.
        |intervals| intervals.color.clone(), // If the config file exists, it returns the color
                                      // from there.
    );
    let alarm = config
        .as_ref()
        .map_or_else(|| None, |intervals| intervals.alarm.clone());

    match read_cli() {
        Ok(intervals) => Some(Intervals {
            short: intervals.short,
            long: intervals.long,
            color,
            alarm,
        }),
        Err(true) => config,
        Err(false) => None,
    }
}

pub fn notify_time(done: (u8, u8, u8), next: (u8, u8, u8)) {
    let body: String = format!(
        "The {} interval is over.\n\nClose the notification to start the next {} interval.",
        stylize_text(done),
        stylize_text(next),
    );

    let alarm = match get_intervals() {
        // If the config file exists
        Some(intervals) => match intervals.alarm {
            // And it has a sound defined
            Some(path) => match File::open(path) {
                // And the sound file can be opened,
                Ok(file) => Some(file), // Return Some(file)
                Err(_) => None,         // Otherwise return None
            },
            None => None,
        },
        None => None,
    };
    
    match alarm {
        // If the alarm sound file exists, create a notification with a sound running.
        // This is done like this because the audio thread only exists while stream_handle is in
        // scope. If I made this into a separate function, the sound wouldn't have time to play.
        // This way, the stream_handle is in scope as long as the notification hasn't been closed.
        Some(alarm) => {
            // If the user has defined a , create a notification with the sound playing.
            let (_stream, stream_handle) = OutputStream::try_default().unwrap();
            let file = BufReader::new(alarm);
            let source = Decoder::new(file).unwrap();
            stream_handle.play_raw(source.convert_samples()).unwrap();

            let notification_handler = Notification::new()
                .summary("Tomaquet")
                .body(&body)
                .urgency(Urgency::Critical)
                .timeout(Timeout::Never)
                .show();

            match notification_handler {
                Ok(handler) => handler.on_close(|| {}),
                Err(_) => (),
            }
        }
        // If the file doesn't exist or wasn't defined, create a notification without sound.
        None => {
            let notification_handler = Notification::new()
                .summary("Tomaquet")
                .body(&body)
                .urgency(Urgency::Critical)
                .timeout(Timeout::Never)
                .show();

            match notification_handler {
                Ok(handler) => handler.on_close(|| {}),
                Err(_) => (),
            }
        }
    }
}

pub fn get_seconds(time: (u8, u8, u8)) -> i64 {
    i64::from(time.0) * 3600 + i64::from(time.1) * 60 + i64::from(time.2)
}

pub fn get_color(color: Option<String>) -> Color {
    color.map_or(DEF_COLOR_ENUM, |color_str| {
        match color_str.to_lowercase().as_str() {
            "black" => Color::Black,
            "green" => Color::Green,
            "yellow" => Color::Yellow,
            "blue" => Color::Blue,
            "magenta" => Color::Magenta,
            "cyan" => Color::Cyan,
            "darkgray" => Color::DarkGray,
            "lightred" => Color::LightRed,
            "lightgreen" => Color::LightGreen,
            "lightyellow" => Color::LightYellow,
            "lightblue" => Color::LightBlue,
            "lightmagenta" => Color::LightMagenta,
            "lightcyan" => Color::LightCyan,
            "white" => Color::White,
            _ => DEF_COLOR_ENUM,
        }
    })
}
