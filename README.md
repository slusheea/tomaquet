![demo](images/demo.gif)

# Tomàquet
A pomodoro timer, but I'm not Italian so tomàquet it is.

Tomaquet allows you to set two intervals it will cycle between indefinitely in two different ways: Through the command line or with the config file.

![responsive](images/responsive.png)
Fully responsive design with notification and alarm sound.

### Cli usage
```bash
tomaquet [LONG_INTERVAL] [SHORT INTERVAL]
```
The intervals are formatted as hh:mm:ss, so to set the classic 25-5 interval:
```bash
tomaquet 00:25:00 00:05:00
```

### Config file
If no interval is specified via CLI, tomaquet will use those on the config file.
```toml
# interval = [hours, minutes, seconds]
long = [0, 30, 0]
short = [0, 5, 0]
color = "cyan"
alarm = "/home/slushee/Music/alarm.mp3"
```
Available colors are [here](https://docs.rs/ratatui/latest/ratatui/style/enum.Color.html). Capitalization doesn't matter.
The file will be created when tomaquet is ran for the first time.

### Shortcuts
- `p` or `space` -> Pause/Resume
- `f` -> Finish interval
- `q` -> Quit

### Troubleshooting
You might need to have `alsa-utils`, `alsa-lib`, `alsa-lib-devel`, `apulse` or `alsa-plugins-pulseaudio` for the alarm to work.

You need to have a properly configured notification daemon with access to the XDG variables for the notifications to work.

I provide an alarm sound if you don't want to go searching for one. It's on the `resources` folder. If no sound is specified on the config file, it simply won't play any alarm sound
